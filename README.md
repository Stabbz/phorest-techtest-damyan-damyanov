# Phorest Software Engineer Technical Test

**System Prerequisites:** Java 21
**Optional:** Docker, PostgreSQL

## Build and run

* With local PostgreSQL -> Build and run in one -> Linux/Unix: `./gradlew bootRun`, Windows PowerShell: `.\gradlew bootRun`
* With docker-compose -> Build -> `./gradlew clean build` -> `docker build .` -> `docker-compose up`
Note: when running with docker, app port is mapped to `8085` and db port is mapped to `5435`

## Swagger Documentation
The endpoints exposed by this app can be viewed on "http://localhost:8087/swagger-ui/index.html"

## Notes
I've modeled the database 1:1 with the input files. 
Since services and purchases have the same structure, another approach would be to maintain types of services and "products" 
in two different tables and have purchases contain references to services and products used in an appointment.
But since that's not stated explicitly in the requirements, I've opted to not go for it. 
In a real world scenario, I would go for this approach after consulting with the person requesting the change (PO, BA, etc.).
