package com.phorest.damyan.service;

import com.phorest.damyan.service.model.Failure;
import com.phorest.damyan.service.model.Success;
import io.vavr.control.Either;

import java.io.IOException;
import java.io.InputStream;

public interface ImportService {

    Either<Failure, Success> importFile(String fileName, InputStream is) throws IOException;
}
