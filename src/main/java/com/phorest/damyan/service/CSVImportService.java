package com.phorest.damyan.service;

import com.phorest.damyan.persistence.model.Appointment;
import com.phorest.damyan.persistence.model.Client;
import com.phorest.damyan.persistence.model.Purchase;
import com.phorest.damyan.persistence.repository.AppointmentRepository;
import com.phorest.damyan.persistence.repository.ClientRepository;
import com.phorest.damyan.persistence.repository.PurchaseRepository;
import com.phorest.damyan.persistence.repository.ServiceRepository;
import com.phorest.damyan.service.model.Failure;
import com.phorest.damyan.service.model.FileType;
import com.phorest.damyan.service.model.Success;
import com.phorest.damyan.util.DateUtil;
import io.vavr.control.Either;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.phorest.damyan.service.model.FileType.*;

@Service
public class CSVImportService implements ImportService {

    private final ClientRepository clientRepository;
    private final AppointmentRepository appointmentRepository;
    private final PurchaseRepository purchaseRepository;
    private final ServiceRepository serviceRepository;

    private final String headerErrorStr = "Headers don't match file name/type.";

    @Autowired
    public CSVImportService(ClientRepository clientRepository, AppointmentRepository appointmentRepository,
                            PurchaseRepository purchaseRepository, ServiceRepository serviceRepository) {
        this.clientRepository = clientRepository;
        this.appointmentRepository = appointmentRepository;
        this.purchaseRepository = purchaseRepository;
        this.serviceRepository = serviceRepository;
    }

    @Override
    public Either<Failure, Success> importFile(String fileName, InputStream is) throws IOException {
        Optional<FileType> typeOpt = FileType.fromString(fileName);
        if (typeOpt.isPresent()) {
            try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
                CSVFormat format = CSVFormat.DEFAULT.builder()
                        .setDelimiter(',')
                        .setHeader()
                        .build();
                CSVParser parser = new CSVParser(fileReader, format);

                return processLines(parser.getHeaderNames(), parser.getRecords(), typeOpt.get());
            }
        } else {
            return Either.left(new Failure("Unsupported file name/type."));
        }
    }

    private Either<Failure, Success> processLines(List<String> headers, List<CSVRecord> lines, FileType type) {
        //possible improvement: maybe try/catch individual records, insert all that are okay,
        // collect errors on failed ones and return failing row nums, maybe use Vavr library
        try {
            switch (type) {
                case PURCHASES -> {
                    // header check can be extracted to a method, but the handling of that method result
                    // would require even more "duplicate" code
                    if (!headers.equals(PURCHASES.getHeaders())) {
                        return Either.left(new Failure(headerErrorStr));
                    }
                    var results = lines.stream()
                            .map(record ->
                                    new Purchase(
                                            UUID.fromString(record.get("id")),
                                            record.get("name"),
                                            Double.parseDouble(record.get("price")),
                                            Integer.parseInt(record.get("loyalty_points")),
                                            UUID.fromString(record.get("appointment_id"))))
                            .toList();
                    purchaseRepository.saveAll(results);
                }
                case APPOINTMENTS -> {
                    if (!headers.equals(APPOINTMENTS.getHeaders())) {
                        return Either.left(new Failure(headerErrorStr));
                    }
                    var results = lines.stream()
                            .map(record ->
                                    new Appointment(
                                            UUID.fromString(record.get("id")),
                                            DateUtil.strToInstant(record.get("start_time")),
                                            DateUtil.strToInstant(record.get("end_time")),
                                            UUID.fromString(record.get("client_id"))))
                            .toList();
                    appointmentRepository.saveAll(results);
                }
                case CLIENTS -> {
                    if (!headers.equals(CLIENTS.getHeaders())) {
                        return Either.left(new Failure(headerErrorStr));
                    }
                    var results = lines.stream()
                            .map(record ->
                                    new Client(
                                            UUID.fromString(record.get("id")),
                                            record.get("first_name"),
                                            record.get("last_name"),
                                            record.get("email"),
                                            record.get("phone"),
                                            record.get("gender"),
                                            Boolean.parseBoolean(record.get("banned"))))
                            .toList();
                    clientRepository.saveAll(results);
                }
                case SERVICES -> {
                    if (!headers.equals(SERVICES.getHeaders())) {
                        return Either.left(new Failure(headerErrorStr));
                    }
                    var results = lines.stream()
                            .map(record ->
                                    new com.phorest.damyan.persistence.model.Service(
                                            UUID.fromString(record.get("id")),
                                            record.get("name"),
                                            Double.parseDouble(record.get("price")),
                                            Integer.parseInt(record.get("loyalty_points")),
                                            UUID.fromString(record.get("appointment_id"))))
                            .toList();
                    serviceRepository.saveAll(results);
                }

            }
        } catch (Exception e) {
            return Either.left(new Failure("Failed to process records for " + type.getName() + " type"));
        }
        return Either.right(new Success("Successfully imported file: " + type.getName()));
    }
}
