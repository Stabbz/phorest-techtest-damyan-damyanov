package com.phorest.damyan.service.model;

public record Failure(String message) implements Result {
}
