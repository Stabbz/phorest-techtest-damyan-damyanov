package com.phorest.damyan.service.model;

public sealed interface Result permits Failure, Success {
}
