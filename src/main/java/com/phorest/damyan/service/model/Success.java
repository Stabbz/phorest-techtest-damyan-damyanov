package com.phorest.damyan.service.model;

public record Success(String message) implements Result {
}
