package com.phorest.damyan.service.model;

import java.util.List;
import java.util.Optional;

public enum FileType {
    PURCHASES("purchases.csv", List.of("id", "appointment_id", "name", "price", "loyalty_points")),
    APPOINTMENTS("appointments.csv", List.of("id", "client_id", "start_time", "end_time")),
    CLIENTS("clients.csv", List.of("id", "first_name", "last_name", "email", "phone", "gender", "banned")),
    SERVICES("services.csv", List.of("id", "appointment_id", "name", "price", "loyalty_points"));

    private final String name;
    private final List<String> headers;

    FileType(String name, List<String> headers) {
        this.name = name;
        this.headers = headers;
    }

    public static Optional<FileType> fromString(String str) {
        for (FileType ft : FileType.values()) {
            if (ft.getName().equalsIgnoreCase(str)) {
                return Optional.of(ft);
            }
        }
        return Optional.empty();
    }

    public String getName() {
        return name;
    }

    public List<String> getHeaders() {
        return headers;
    }
}
