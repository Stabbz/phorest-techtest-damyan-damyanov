package com.phorest.damyan.persistence.repository;

import com.phorest.damyan.persistence.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public interface ClientRepository extends JpaRepository<Client, UUID> {

    @Query(
            value = """
                    with sums as (
                    	with cl as (select c.id as client_id, c.first_name, c.last_name, c.email, c.gender, c.phone, c.banned, a.id as appointment_id, a.start_time, a.end_time from clients c
                    					join appointments a on c.id = a.client_id
                    					where a.end_time >= ?2
                    					and c.banned is false),
                    	pur as (select p.appointment_id, sum(p.loyalty_points) total_points from purchases p
                    				group by appointment_id),
                    	ser as (select s.appointment_id, sum(s.loyalty_points) total_points from services s
                    				group by appointment_id)
                    	select c.client_id as id, c.first_name as firstName, c.last_name as lastName, c.email, c.gender,
                    	 c.phone, c.banned, sum(p.total_points) + sum(s.total_points) as totalLoyaltyPoints from cl c
                    	left join pur p on p.appointment_id = c.appointment_id
                    	left join ser s on s.appointment_id = c.appointment_id
                    	group by c.client_id, c.first_name, c.last_name, c.email, c.gender, c.phone, c.banned
                    )
                    select * from sums
                    where totalLoyaltyPoints > 0
                    order by totalLoyaltyPoints desc
                    limit ?1""",
            nativeQuery = true
    )
    List<ClientLoyalty> findMostLoyalClients(Integer count, Date since);

    interface ClientLoyalty {
        String getFirstName();
        String getLastName();
        String getEmail();
        String getGender();
        String getPhone();
        Boolean getBanned();
        Long getTotalLoyaltyPoints();
    }
}
