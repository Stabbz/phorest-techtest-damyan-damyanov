package com.phorest.damyan.persistence.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "appointments")
public class Appointment {

    @Id
    @NotNull
    private UUID id;
    private Instant startTime;
    private Instant endTime;
    @Column(name = "client_id")
    private UUID clientId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", insertable = false, updatable = false)
    private Client client;
    @OneToMany(mappedBy = "appointment", fetch = FetchType.LAZY)
    private List<Service> services;
    @OneToMany(mappedBy = "appointment", fetch = FetchType.LAZY)
    private List<Purchase> purchases;

    public Appointment() {
    }

    public Appointment(UUID id, Instant startTime, Instant endTime, UUID clientId) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.clientId = clientId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public UUID getClientId() {
        return clientId;
    }

    public void setClientId(UUID clientId) {
        this.clientId = clientId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<Purchase> purchases) {
        this.purchases = purchases;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Appointment that = (Appointment) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getStartTime() != null ? !getStartTime().equals(that.getStartTime()) : that.getStartTime() != null)
            return false;
        if (getEndTime() != null ? !getEndTime().equals(that.getEndTime()) : that.getEndTime() != null) return false;
        if (getClientId() != null ? !getClientId().equals(that.getClientId()) : that.getClientId() != null)
            return false;
        if (getClient() != null ? !getClient().equals(that.getClient()) : that.getClient() != null) return false;
        if (getServices() != null ? !getServices().equals(that.getServices()) : that.getServices() != null)
            return false;
        return getPurchases() != null ? getPurchases().equals(that.getPurchases()) : that.getPurchases() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getStartTime() != null ? getStartTime().hashCode() : 0);
        result = 31 * result + (getEndTime() != null ? getEndTime().hashCode() : 0);
        result = 31 * result + (getClientId() != null ? getClientId().hashCode() : 0);
        result = 31 * result + (getClient() != null ? getClient().hashCode() : 0);
        result = 31 * result + (getServices() != null ? getServices().hashCode() : 0);
        result = 31 * result + (getPurchases() != null ? getPurchases().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", clientId=" + clientId +
                ", client=" + client +
                ", services=" + services +
                ", purchases=" + purchases +
                '}';
    }
}
