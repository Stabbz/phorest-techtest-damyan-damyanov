package com.phorest.damyan.persistence.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

@Entity
@Table(name = "purchases")
public class Purchase {

    @Id
    @NotNull
    private UUID id;
    private String name;
    private Double price;
    @Column(name = "loyalty_points")
    private Integer loyaltyPoints;
    @Column(name = "appointment_id")
    private UUID appointmentId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "appointment_id", insertable = false, updatable = false)
    private Appointment appointment;

    public Purchase() {
    }

    public Purchase(UUID id, String name, Double price, Integer loyaltyPoints, UUID appointmentId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.loyaltyPoints = loyaltyPoints;
        this.appointmentId = appointmentId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(Integer loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public UUID getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(UUID appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Purchase purchase = (Purchase) o;

        if (getId() != null ? !getId().equals(purchase.getId()) : purchase.getId() != null) return false;
        if (getName() != null ? !getName().equals(purchase.getName()) : purchase.getName() != null) return false;
        if (getPrice() != null ? !getPrice().equals(purchase.getPrice()) : purchase.getPrice() != null) return false;
        if (getLoyaltyPoints() != null ? !getLoyaltyPoints().equals(purchase.getLoyaltyPoints()) : purchase.getLoyaltyPoints() != null)
            return false;
        if (getAppointmentId() != null ? !getAppointmentId().equals(purchase.getAppointmentId()) : purchase.getAppointmentId() != null)
            return false;
        return getAppointment() != null ? getAppointment().equals(purchase.getAppointment()) : purchase.getAppointment() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getPrice() != null ? getPrice().hashCode() : 0);
        result = 31 * result + (getLoyaltyPoints() != null ? getLoyaltyPoints().hashCode() : 0);
        result = 31 * result + (getAppointmentId() != null ? getAppointmentId().hashCode() : 0);
        result = 31 * result + (getAppointment() != null ? getAppointment().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", loyaltyPoints=" + loyaltyPoints +
                ", appointmentId=" + appointmentId +
                ", appointment=" + appointment +
                '}';
    }
}
