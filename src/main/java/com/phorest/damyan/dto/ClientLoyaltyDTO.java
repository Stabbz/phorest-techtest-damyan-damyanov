package com.phorest.damyan.dto;

public record ClientLoyaltyDTO(String firstName, String lastName, String email, String phone, String gender, Boolean banned, Long totalLoyaltyPoints) {}
