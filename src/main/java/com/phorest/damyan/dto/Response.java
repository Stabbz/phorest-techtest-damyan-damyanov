package com.phorest.damyan.dto;

public sealed interface Response permits SuccessResponse, ErrorResponse{
}
