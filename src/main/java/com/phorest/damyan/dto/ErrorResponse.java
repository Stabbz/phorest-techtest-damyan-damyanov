package com.phorest.damyan.dto;

public record ErrorResponse(Integer errorCode, String message) implements Response {
}
