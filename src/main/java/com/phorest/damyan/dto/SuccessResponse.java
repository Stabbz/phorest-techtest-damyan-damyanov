package com.phorest.damyan.dto;

public record SuccessResponse(String message) implements Response {
}
