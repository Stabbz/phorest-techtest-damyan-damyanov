package com.phorest.damyan.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;

public record ClientDTO(
        String firstName,
        String lastName,
        @Email(message="Please provide a valid email address") @Schema(example = "john@doe.com") @Valid String email,
        String phone,
        String gender,
        Boolean banned) {}
