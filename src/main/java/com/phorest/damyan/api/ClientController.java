package com.phorest.damyan.api;

import com.phorest.damyan.dto.ClientDTO;
import com.phorest.damyan.dto.ClientLoyaltyDTO;
import com.phorest.damyan.persistence.model.Client;
import com.phorest.damyan.persistence.repository.ClientRepository;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/clients")
public class ClientController {

    private final ClientRepository repository;

    @Autowired
    public ClientController(ClientRepository repository) {
        this.repository = repository;
    }

    @GetMapping(path = "/most-loyal", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description =
            """
            Endpoint returns the top <b>{count}</b> of clients that have accumulated the most loyalty points\040
            from <b>{since}</b> date until now.
            Banned clients are excluded.
            """)
    public ResponseEntity<List<ClientLoyaltyDTO>> mostLoyalClients(
            @RequestParam(value = "count", defaultValue = "50")
            Integer count,
            @RequestParam(value = "since", defaultValue = "2010-10-10")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            Date since) {

        var result = repository.findMostLoyalClients(count, since).stream()
                .map(cl -> new ClientLoyaltyDTO(
                        cl.getFirstName(),
                        cl.getLastName(),
                        cl.getEmail(),
                        cl.getPhone(),
                        cl.getGender(),
                        cl.getBanned(),
                        cl.getTotalLoyaltyPoints()))
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(result);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Client createClient(@Valid @RequestBody ClientDTO clientDTO) {
        Client client = new Client(
                UUID.randomUUID(),
                clientDTO.firstName(),
                clientDTO.lastName(),
                clientDTO.email(),
                clientDTO.phone(),
                clientDTO.gender(),
                clientDTO.banned()
        );
        return repository.save(client);
    }

    @PatchMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Client updateClient(@Valid @RequestBody Client client) {
        return repository.save(client);
    }

    @DeleteMapping(path = "/{clientId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteClient(@PathVariable("clientId") UUID clientId) {
        repository.deleteById(clientId);
    }

    @GetMapping(path = "/{clientId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> fetchClientById(@PathVariable("clientId") UUID clientId) {
        Optional<Client> clientOpt = repository.findById(clientId);
        return clientOpt.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.ok().build());
    }

    @GetMapping(path = "/page")
    public Page<Client> fetchClients(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
