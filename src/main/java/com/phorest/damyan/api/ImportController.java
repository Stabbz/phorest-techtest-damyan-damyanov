package com.phorest.damyan.api;

import com.phorest.damyan.dto.ErrorResponse;
import com.phorest.damyan.dto.Response;
import com.phorest.damyan.dto.SuccessResponse;
import com.phorest.damyan.service.ImportService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.phorest.damyan.api.ErrorCode.*;
import static com.phorest.damyan.util.CSVUtil.hasCSVFormat;
import static io.vavr.API.*;
import static io.vavr.Patterns.$Left;
import static io.vavr.Patterns.$Right;

@RestController
@RequestMapping("/import")
public class ImportController {

    private final ImportService importService;

    @Autowired
    public ImportController(ImportService importService) {
        this.importService = importService;
    }

    @PostMapping(path = "/", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(description =
    """
    Endpoint accepts only 4 types of files with these exact names,
    in this order of import: <b>clients.csv, appointments.csv, services.csv, purchases.csv</b>.
    
    <br>
    Out of order imports will fail.
    """)
    public ResponseEntity<Response> handleFileImport(@RequestParam("file") MultipartFile file) throws IOException {
        if (!hasCSVFormat(file)) {
            return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).body(new ErrorResponse(CSV_ONLY.getCode(), CSV_ONLY.getDescription()));
        }
        var result = importService.importFile(file.getOriginalFilename(), file.getInputStream());
        // trying to use pattern matching on the Either left/right as it would be done in Scala,
        // syntax doesn't look great with the $ signs, maybe not the best idea
        return Match(result).of(
                Case($Right($()), success ->
                        ResponseEntity.status(HttpStatus.OK)
                                .body(new SuccessResponse("Import successful." + success.message()))),
                Case($Left($()), failure ->
                        ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                                .body(new ErrorResponse(GENERAL_IMPORT_FAIL.getCode(), GENERAL_IMPORT_FAIL.getDescription() + failure.message())))
        );
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(IOException.class)
    public ErrorResponse handleIOException(IOException exc) {
        return new ErrorResponse(UNEXPECTED_ERROR.getCode(), UNEXPECTED_ERROR.getDescription());
    }
}
