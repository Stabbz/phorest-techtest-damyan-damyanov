package com.phorest.damyan.api;

public enum ErrorCode {
    GENERAL_IMPORT_FAIL(1, "Error while trying to import file."),
    CSV_ONLY(2, "Only CSV files are accepted (text/csv)."),
    UNEXPECTED_ERROR(3, "Unexpected error.");

    private final int code;
    private final String description;

    ErrorCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + ": " + description;
    }

}
