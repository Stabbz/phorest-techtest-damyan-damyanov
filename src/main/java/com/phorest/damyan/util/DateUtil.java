package com.phorest.damyan.util;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    public static Instant strToInstant(String input) {
        String pattern = "yyyy-MM-dd HH:mm:ss Z";
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(input, dateTimeFormatter);
        return zonedDateTime.toInstant();
    }

}
