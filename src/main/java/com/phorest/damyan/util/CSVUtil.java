package com.phorest.damyan.util;

import org.springframework.web.multipart.MultipartFile;

public class CSVUtil {

    public static boolean hasCSVFormat(MultipartFile file) {
        return "text/csv".equals(file.getContentType());
    }

}
