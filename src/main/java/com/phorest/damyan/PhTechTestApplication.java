package com.phorest.damyan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhTechTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhTechTestApplication.class, args);
	}

}
