package com.phorest.damyan.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.phorest.damyan.dto.ClientDTO;
import com.phorest.damyan.persistence.model.Client;
import com.phorest.damyan.persistence.repository.ClientRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
public class ClientControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ClientRepository clientRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final String baseUrl = "/clients";

    @Test
    void shouldOkOnValidRequest() throws Exception {
        ClientDTO clientDTO = new ClientDTO("fname", "lname", "eme@m.com", "123", "m", false);
        Client client = new Client();
        client.setFirstName(clientDTO.firstName());
        doReturn(client).when(clientRepository).save(any());

        mvc.perform(post(baseUrl).contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(clientDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is(clientDTO.firstName())));
    }

    @Test
    void shouldValidateEmailOnCreate() throws Exception {
        ClientDTO clientDTO = new ClientDTO("fname", "lname", "ememcom", "123", "m", false);

        mvc.perform(post(baseUrl).contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(clientDTO)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should400UpdateOnNulls() throws Exception {
        Client client = new Client();

        mvc.perform(patch(baseUrl).contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(client)))
                .andExpect(status().is4xxClientError());
    }
}
