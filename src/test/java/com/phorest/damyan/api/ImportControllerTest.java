package com.phorest.damyan.api;

import com.phorest.damyan.service.ImportService;
import com.phorest.damyan.service.model.Failure;
import com.phorest.damyan.service.model.Success;
import io.vavr.control.Either;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static com.phorest.damyan.service.model.FileType.CLIENTS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class ImportControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ImportService importService;

    @Test
    void shouldOKonCSVUpload() throws Exception {
        doReturn(Either.right(new Success("Successfully imported file: " + CLIENTS.getName()))).when(importService).importFile(any(), any());
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.csv",
                "text/csv", "Test".getBytes());
        mvc.perform(multipart("/import/").file(multipartFile))
                .andExpect(status().isOk());
    }

    @Test
    void should415onNonCSVUpload() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.csv",
                "text/plain", "Test".getBytes());
        mvc.perform(multipart("/import/").file(multipartFile))
                .andExpect(status().isUnsupportedMediaType());
    }

    @Test
    void should422onImportFailure() throws Exception {
        doReturn(Either.left(new Failure("Failed to process records for " + CLIENTS.getName() + " type"))).when(importService).importFile(any(), any());
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.csv",
                "text/csv", "Test".getBytes());
        mvc.perform(multipart("/import/").file(multipartFile))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void should500onIOException() throws Exception {
        doThrow(new IOException()).when(importService).importFile(any(), any());
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.csv",
                "text/csv", "Test".getBytes());
        mvc.perform(multipart("/import/").file(multipartFile))
                .andExpect(status().isInternalServerError());
    }

}