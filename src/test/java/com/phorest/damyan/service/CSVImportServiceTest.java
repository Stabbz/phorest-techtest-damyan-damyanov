package com.phorest.damyan.service;

import com.phorest.damyan.persistence.repository.AppointmentRepository;
import com.phorest.damyan.persistence.repository.ClientRepository;
import com.phorest.damyan.persistence.repository.PurchaseRepository;
import com.phorest.damyan.persistence.repository.ServiceRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.phorest.damyan.service.model.FileType.SERVICES;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

class CSVImportServiceTest {

    @Mock
    private ClientRepository clientRepository;
    @Mock
    private AppointmentRepository appointmentRepository;
    @Mock
    private PurchaseRepository purchaseRepository;
    @Mock
    private ServiceRepository serviceRepository;
    @InjectMocks
    private CSVImportService importService;
    private AutoCloseable closeable;

    @BeforeEach
    void initService() {
        closeable = MockitoAnnotations.openMocks(this);

        doReturn(List.of()).when(serviceRepository).saveAll(any());
        doReturn(List.of()).when(purchaseRepository).saveAll(any());
        doReturn(List.of()).when(appointmentRepository).saveAll(any());
        doReturn(List.of()).when(clientRepository).saveAll(any());
    }

    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }

    @BeforeAll
    static void setUp() {
        MockitoAnnotations.openMocks(true);
    }

    @Test
    void shouldProcessAllFilesSuccessfully() throws IOException {
        var fileNames = List.of("clients.csv", "services.csv", "appointments.csv", "purchases.csv");
        for (String fileName : fileNames) {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(fileName);

            var result = importService.importFile(fileName, in);
            assertTrue(result.get().message().startsWith("Successfully imported file: "));
        }
    }

    @Test
    void shouldFailOnNonCSVFile() throws IOException {
        var fileName = "test.txt";
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(fileName);

        var result = importService.importFile(fileName, in);
        assertTrue(result.getLeft().message().startsWith("Unsupported file name/type."));
    }

    @Test
    void shouldFailOnBadData() throws IOException {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("negative/services.csv");

        var result = importService.importFile("services.csv", in);
        assertEquals(result.getLeft().message(), "Failed to process records for " + SERVICES.getName() + " type");
    }

    @Test
    void shouldFailOnBadHeader() throws IOException {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("negative/appointments.csv");

        var result = importService.importFile("appointments.csv", in);
        assertEquals(result.getLeft().message(), "Headers don't match file name/type.");
    }
}