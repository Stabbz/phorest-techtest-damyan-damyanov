package com.phorest.damyan.integration;


import com.phorest.damyan.PhTechTestApplication;
import com.phorest.damyan.persistence.repository.AppointmentRepository;
import com.phorest.damyan.persistence.repository.ClientRepository;
import com.phorest.damyan.persistence.repository.PurchaseRepository;
import com.phorest.damyan.persistence.repository.ServiceRepository;
import com.phorest.damyan.service.CSVImportService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = PhTechTestApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class ClientControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ServiceRepository serviceRepository;
    @Autowired
    private AppointmentRepository appointmentRepository;
    @Autowired
    private PurchaseRepository purchaseRepository;
    @Autowired
    private CSVImportService csvImportService;

    @AfterEach
    void clearDatabase() {
        serviceRepository.deleteAll();
        purchaseRepository.deleteAll();
        appointmentRepository.deleteAll();
        clientRepository.deleteAll();
    }

    @Test
    void shouldReturnEmptyListIfNoResults() throws Exception {
        mvc.perform(get("/clients/most-loyal?count=13&since=2010-10-10")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    void shouldReturnProperResultsFromDbWithFullData() throws Exception {
        populateDb();
        mvc.perform(get("/clients/most-loyal?count=13&since=2010-10-10")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].firstName").value("Christen"))
                .andExpect(jsonPath("$[0].totalLoyaltyPoints").value(965))
                .andExpect(jsonPath("$[1].firstName").value("Roxie"))
                .andExpect(jsonPath("$[1].totalLoyaltyPoints").value(945));
    }

    @Test
    void shouldReturnOnlyOneResultWithDateFilterForGivenData() throws Exception {
        populateDb();
        mvc.perform(get("/clients/most-loyal?count=13&since=2019-02-02")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].firstName").value("Michael"))
                .andExpect(jsonPath("$[0].email").value("ivan@bruen.name"))
                .andExpect(jsonPath("$[0].totalLoyaltyPoints").value(120));
    }

    private void populateDb() throws IOException {
        var fileNames = List.of("clients.csv", "appointments.csv", "services.csv", "purchases.csv");
        for (String fileName : fileNames) {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(fileName);
            csvImportService.importFile(fileName, in);
        }
    }
}
