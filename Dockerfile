FROM azul/zulu-openjdk-alpine:21-latest
MAINTAINER damyan.damyanov

COPY build/libs/ph-tech-test-0.0.1-SNAPSHOT.jar ph-tech-test-0.0.1.jar

ENTRYPOINT ["java", "-jar", "ph-tech-test-0.0.1.jar"]